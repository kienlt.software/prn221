﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Sales> sales;

        public MainWindow()
        {
            InitializeComponent();
            sales = new List<Sales>()
            {
                new Sales{Thang = "Thang 1", LuongThang = 10000, LuongTong = 50000},
                new Sales{Thang = "Thang 2", LuongThang = 12000, LuongTong = 60000},
                new Sales{Thang = "Thang 3", LuongThang = 31000, LuongTong = 55000},
                new Sales{Thang = "Thang 4", LuongThang = 20000, LuongTong = 45000},
                new Sales{Thang = "Thang 5", LuongThang = 23000, LuongTong = 75000},
                new Sales{Thang = "Thang 6", LuongThang = 15000, LuongTong = 90000},
            };
            DataContext = sales;
            //chartColumn.ItemsSource = sales;
        }
    }
}
